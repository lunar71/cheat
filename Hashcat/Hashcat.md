Hashcat cheatsheet
==================

[[_TOC_]]

#### JWT HS256 cracking
##### Mode 16500 with all 6 length characters

    hashcat.bin -m 16500 jwt.txt -a3 -w 3 ?a?a?a?a?a?a --force

