Windows miscellaneous cheatsheet
================================

[[_TOC_]]

#### Powershell mouse jiggler

    function MouseWiggle {
        Add-Type -Assembly System.Windows.Forms
        while($true) {
            Start-Sleep -Seconds 1
            [Windows.Forms.Cursor]::Position = New-Object Drawing.Point (random 1000),(random 1000)
        }
    }

#### LDAP search filter to enumerate LAPS passwords (permissions required)

    ([adsisearcher]'(&(objectCategory=computer)(ms-MCS-AdmPwd=*))').FindAll().Properties

#### Insecure ACEs

    GenericAll              - full control on object
    GenericWrite            - write attributes to an object (e.g. logon script)
    WriteOwner              - change owner of an object
    WriteDACL               - modify object ACEs
    AllExtendedRights       - add user to a group or reset password
    ForceChangePassword     - change user password
    Self (Self-Membership)  - add yourself to group

#### Windows file permissions
A sequence of simple rights

    F (full access)
    M (modify access)
    RX (read and execute access)
    R (read-only access)
    W (write-only access)

A comma-separated list in parenthesis of specific rights

    D (delete)
    RC (read control)
    WDAC (write DAC)
    WO (write owner)
    S (synchronize)
    AS (access system security)
    MA (maximum allowed)
    GR (generic read)
    GW (generic write)
    GE (generic execute)
    GA (generic all)
    RD (read data/list directory)
    WD (write data/add file)
    AD (append data/add subdirectory)
    REA (read extended attributes)
    WEA (write extended attributes)
    X (execute/traverse)
    DC (delete child)
    RA (read attributes)
    WA (write attributes)

Inheritance rights may precede either Perm form, and they are applied only to directories

    (OI): object inherit
    (CI): container inherit
    (IO): inherit only
    (NP): do not propagate inherit
