Windows Shell cheatsheet
========================

[[_TOC_]]

#### Get WiFi passwords

    netsh wlan show profiles
    netsh wlan show profile=<WiFi network name> key=clear
