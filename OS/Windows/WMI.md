WMI cheatsheet
==============

[[_TOC_]]

#### Query the root namespace for classes

    Get-WmiObject -Namespace "root" -Class "__Namespace"
    Get-WmiObject -Namespace "root" -Class "__Namespace" | Select Name

#### Query classes in namespace

    Get-WmiObject -Namespace "root\cimv2" -Class "*" -List

#### Find a class in the default cimv2 namespace by keyword

    Get-WmiObject -Namespace "root\cimv2" -Class "*service*" -List
    Get-WmiObject -Namespace "root\cimv2" -Class "*bios*" -List

#### Query dynamic classes which can be invoked

    Get-CimClass -QualifierName "dynamic"

#### Query a specific class

    Get-WmiObject -Class Win32_BIOS
    Get-CimInstance -ClassName Win32_BIOS

#### Get all methods named "Create" in the "root\cimv2" namespace

     Get-WmiObject -Namespace "root\cimv2" -List | ForEach-Object { $_ | Select -ExpandProperty Methods | Where Name -eq "Create" | Select Origin }

#### Get running processes

    Get-WmiObject -Class Win32_Process -Filter "Name = 'explorer.exe'"

    Get-WmiObject -Query "select * from Win32_Process where Name = 'explorer.exe'" | Select-Object *
    Get-CimInstance -Query "select * from Win32_Process where Name = 'explorer.exe'" | Select-Object *

    Get-WmiObject -Query "select * from Win32_Process where Name = 'explorer.exe'" | Select-Object Name,Path
    Get-CimInstance -Query "select * from Win32_Process where Name = 'explorer.exe'" | Select-Object Name,Path

#### Get running services

    Get-WmiObject -Query "select Name from Win32_Service where State = 'Running'" | Select-Object *
    Get-CimInstance -Query "select Name from Win32_Service where State = 'Running'" | Select-Object *

    Get-WmiObject -Query "select Name from Win32_Service where State = 'Running'" | Select-Object Name, PathName
    Get-CimInstance -Query "select Name from Win32_Service where State = 'Running'" | Select-Object Name, PathName

#### Get logged on users

    Get-WmiObject -Query "select * from Win32_LoggedOnUser"

#### Get log files on disk

    Get-WmiObject -Query "select * from Win32_NTEventlogFile"

#### Remove process

    Get-WmiObject -Class Win32_Process -Filter "Name = 'calculator.exe'" | Remove-WmiObject
    Get-WmiObject -Query "select * from Win32_Process where Name = 'calculator.exe'" | Remove-WmiObject

    Get-CimInstance -ClassName Win32_Process -Filter "Name = 'calculator.exe'" | Remove-CimInstance
    Get-CimInstance -Query "select * from Win32_Process where Name = 'calculator.exe'" | Remove-CimInstance

#### Get classes with methods

    Get-WmiObject * -List | Where-Object {$_.Methods}
    Get-CimClass -MethodName *
    Get-CimClass -MethodName *Create*

#### Explore class methods

    Get-WmiObject -Class Win32_Process -List | Select -ExpandProperty Methods
    (Get-WmiObject -Class Win32_Process -List).Methods

    Get-CimClass -ClassName Win32_Process | Select -ExpandProperty CimClassMethods
    (Get-CimClass -ClassName Win32_Process).CimClassMethods

#### Explore class method parameters

    Get-CimClass -ClassName Win32_Process | Select -ExpandProperty CimClassMethods | Where name -eq "Create" | Select -ExpandProperty Parameters

#### Use methods

    Invoke-WmiMethod -Class Win32_Process -Name Create -ArgumentList calc.exe
    Invoke-WmiMethod -Class Win32_Process -Name Create -ArgumentList @(calc.exe)

    Invoke-CimMethod -ClassName Win32_Process -Name Create -ARguments ${CommandLine = "calc.exe"}

#### Update WMI objects

    Get-WmiObject -Class Win32_Printer -Filter "Name = 'Microsoft XPS Document Writer'" | Set-WmiInstance -Arguments @{Comment = "<comment>"}
    Get-CimInstance -ClassName Win32_Printer -Filter "Name = 'Microsoft XPS Document Writer'" | Set-CimInstance -Property @{Comment = "<comment>"}
    Invoke-WmiMethos -Class Win32_Process -Name Create -ArgumentList @(calc.exe)

#### Get all process owner

    Get-WmiObject Win32_Process | Select-Object Name,@{n="Owner";e={$_.GetOwner().User}}
