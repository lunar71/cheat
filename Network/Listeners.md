Listeners cheatsheet
====================

[[_TOC_]]

#### Bash
##### netcat

    nc -lvp 443

##### rlwrap and netcat

    rlwrap nc -lvp 443

##### socat

    socat -d -d PCT-LISTEN:443 STDOUT

##### socat TTY

    socat -d -d file:`tty`,raw,echo=0 TCP-LISTEN:443

##### netcat web server
Always HTTP 200

    ncat --keep-open -l -p 1337 -c "printf 'HTTP/1.1 200 OK\r\n\r\n'; cat /tmp/evil.html"

With restart

    while :; do \
        (echo -ne "HTTP/1.1 200 OK\r\nContent-Length: $(wc -c </tmp/evil.html)\r\n\r\n"; cat /tmp/evil.html) | ncat -l -p 1337; \
    done

#### Python
##### Python Twisted HTTP web server

    python3 -c "from sys import stdout; \
        from twisted.web.server import Site; \
        from twisted.web.static import File; \
        from twisted.internet import reactor; \
        from twisted.python import log; \
        reactor.listenTCP(8000, Site(File('.'))); log.startLogging(stdout); reactor.run()"

##### Python Twisted HTTPS web server

    openssl req -nodes -new -x509 -keyout /tmp/https.key -out /tmp/https.crt -subj "/C=US/ST=State/L=Cityopia/O=Company/CN=localhost" > /dev/null 2>&1
    python3 -c "from sys import stdout; \
        from twisted.internet import endpoints, reactor, ssl; \
        from twisted.web.static import File; \
        from twisted.web.server import Site; \
        from twisted.python import log; \
        certificate = ssl.DefaultOpenSSLContextFactory('/tmp/https.key', '/tmp/https.crt'); \
        https_server = endpoints.SSL4ServerEndpoint(reactor, 8000, certificate, interface='0.0.0.0'); \
        https_server.listen(Site(File('.'))); \
        log.startLogging(stdout); \
        reactor.run()" && \

    rm /tmp/https.key /tmp/https.crt

##### Python WebDav HTTPS server

    echo "{\"ssl_certificate\": \"/tmp/webdavs.crt\", \"ssl_private_key\": \"/tmp/webdavs.key\"}" > /tmp/webdavs.json
    openssl req -nodes -new -x509 -keyout /tmp/webdavs.key -out /tmp/webdavs.crt -subj "/C=US/ST=State/L=Cityopia/O=Company/CN=localhost" > /dev/null 2>&1
    wsgidav --host=0.0.0.0 --port=8000 --auth=anonymous --root=$(pwd) --config /tmp/webdavs.json && rm /tmp/webdavs.key /tmp/webdavs.crt /tmp/webdavs.json


##### Python FTP server
Requires pyftpdlib

    python -c "
    from pyftpdlib.authorizers import DummyAuthorizer
    from pyftpdlib.handlers import FTPHandler
    from pyftpdlib.servers import FTPServer

    authorizer = DummyAuthorizer()
    authorizer.add_anonymous('.')

    handler = FTPHandler
    handler.authorizer = authorizer

    server = FTPServer(('', 21), handler)
    server.serve_forever()
    "

