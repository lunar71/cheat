HTTP X-Headers
==============

[[_TOC_]]

Header fuzzing
==============

#### More on [SecLists lowercase wordlist](https://github.com/danielmiessler/SecLists/blob/master/Discovery/Web-Content/BurpSuite-ParamMiner/lowercase-headers)
#### More on [SecLists uppercase wordlist](https://github.com/danielmiessler/SecLists/blob/master/Discovery/Web-Content/BurpSuite-ParamMiner/uppercase-headers)

    X-Fowarded-Host
    X-Forwarded-Port
    X-Forwarded-Scheme
    X-Forwarded-For
    X-Frame-Options
    X-Client-IP
    Client-IP
    Origin
    X-Originating-IP
    X-Remote-IP
    X-Remote-Addr
