#!/bin/bash

md="WSTG_Cheatsheet_$(date "+%d-%m-%Y_%H%M").md"
csv="WSTG_Cheatsheet_$(date "+%d-%m-%Y_%H%M").csv"
org="WSTG_Cheatsheet_$(date "+%d-%m-%Y_%H%M").org"


curl -sSLk https://raw.githubusercontent.com/OWASP/CheatSheetSeries/master/cheatsheets_excluded/Web_Application_Security_Testing_Cheat_Sheet.md \
    | awk '/The Checklist/{flag=1} /Other Formats/{flag=0} flag' \
    | sed 's/[][]//g; s/(.*)//g' \
    > $md

cat $md | grep -v 'The Checklist' \
    | sed 's/--.*//g; 
           s/==.*//g;
           /^[[:space:]]*$/d;
           s/&gt;/>/g;
           s/&lt;/</g;
           s/,//g;
           s/$/,/;
           s/^*/-- /;
           s/*//g;
           s/^[[:upper:]]/--- &/;
           /^---/i \ ' \
    | sed '1 i\Checklist,Notes' > $csv

pandoc -f markdown -t org -o $org $md --lua-filter=filter.lua
