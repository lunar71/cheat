iOS mobile application penetration testing cheatsheet
=====================================================
NOTE: tested on iPhone 6 running iOS 12.4.5, checkra1n jailbreak, Cydia64_1.1.32~b24 (en-us)

[[_TOC_]]

Useful repositories
===================

    iPhone:~ root# cat /etc/apt/sources.list.d/cydia.list
    deb https://apt.bingner.com/ ./
    deb http://apt.thebigboss.org/repofiles/cydia/ stable main
    deb https://julioverne.github.io/ ./
    deb http://apt.modmyi.com/ stable main
    deb https://repo.dynastic.co/ ./
    deb http://repo.hackyouriphone.org/ ./
    deb https://repo.chariz.com/ ./
    deb http://cydia.zodttd.com/repo/cydia/ stable main
    deb https://build.frida.re/ ./
    deb http://repo.bingner.com/ ./
    deb http://cydia.iphonecake.com/ ./

Useful packages to install
==========================

    apt update && \
    apt install network-cmds developer-cmds diskdev-cmds \
                file-cmds nano gawk vim make unrar unzip \
                curl openssl git preferenceloader applist \
                com.tigisoftware.filza odcctools ldid \
                com.bingner.plutil com.julioverne.screendump13 \

How to extract IPA from installed application
=============================================

#### Requirements
##### [usbmuxd](https://github.com/TestStudio/usbmuxd)

##### [frida-ios-dump](https://github.com/AloneMonkey/frida-ios-dump)

    pip install -r frida-ios-dump/requirements.txt

#### Dump IPA
##### Forward USB SSH port 22 to localhost on port 2222

    python usbmuxd/python-client/tcprelay.py -t 22:2222

##### Test SSH connection into localhost on port 2222

    $ ssh root@localhost -p 2222
    root@localhost's password: alpine
    Last login: Thu Jan  1 12:00:00 1970 from 127.0.0.1
    iPhone:~ root#

##### Get app name

    $ cat /var/containers/Bundle/Application/<UID>/App Name.app/Info.plist

##### Dump IPA from installed application

    frida-ios-dump/dump.py <MyAppNameWithoutDotApp>


Useful commands
===============

##### Change hostname

    scutil --set HostName x-iPhone

##### Restore DPKG

    cp -r /Library/dpkg /var/lib
