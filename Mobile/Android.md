Android mobile application penetration testing cheatsheet
=========================================================

[[_TOC_]]

Useful tools
============

##### [apktool](https://bitbucket.org/iBotPeaches/apktool/downloads)

##### [dex2jar](https://github.com/pxb1988/dex2jar)

##### [sign](https://github.com/appium/sign)

##### [jd-gui](https://github.com/java-decompiler/jd-gui/releases)

APK manipulation
================

##### Decompile APK

    apktool d app.apk

##### Recompile APK

    apktool b -d app_path output_apk.apk

##### Sign APK

    java -jar sign/dist/signapk.jar sign/testkey.x509.pem sign/testkey.pk8 output_app.apk signed_app.apk

##### Verify APK signature

    jarsigner -verify signed_app.apk

##### Convert .dex to .class

    d2j-dex2jar.sh app.apk

SSL bypass with Frida
=====================
Works for most/typical applications

#### Requirements
##### Frida tools

    pip install frida-tools

##### [Frida server](https://github.com/frida/frida/releases)

    unxz frida-server-<version-<android>-<arch>.xz
    adb push frida-server-<version-<android>-<arch>.xz /data/local/tmp/frida-server

#### Dynamic instrumentation
##### Start Frida server on device

    adb shell 'su -c setenforce 0'
    adb shell 'su -c /data/local/tmp/frida-server &'

##### List running processes

    frida-ps -Uia

##### Bypass common SSL pinning implementations with Frida JS script

    frida -U <ssl-bypass.js> --no-paus -f com.my.app.name


#### Frida SSL bypass scripts

    Java.perform(function() {
        var array_list = Java.use("java.util.ArrayList");
        var ApiClient = Java.use('com.android.org.conscrypt.TrustManagerImpl');
        ApiClient.checkTrustedRecursive.implementation = function(a1, a2, a3, a4, a5, a6) {
            // console.log('Bypassing SSL Pinning');
            var k = array_list.$new();
            return k;
        }
    }, 0);

Drozer
======

#### Requirements
##### [Drozer repo](https://github.com/FSecureLABS/drozer)

    pip install drozer

##### [Drozer agent](https://github.com/FSecureLABS/drozer/releases/tag/2.3.4)

    adb install drozer-agent-2.3.4.apk

#### Drozer guide
##### ADB port forward to Drozer agent
Start Drozer agent application first

    adb forward tcp:31415 tcp:31415

##### Connect to Drozer console

    drozer console connect

##### Determine attack surface

    dz> run app.package.attacksurface com.my.app.name

##### Enumerate exported activities

    dz> run app.activitiy.info -a com.my.app.name

##### Launch exported activity

    dz> run app.activity.start --component com.my.app.name com.my.app.name.ExportedActivity

##### Enumerate exported Broadcast Receivers

    dz> run app.broadcast.info -a com.my.app.name -i
    Package: com.my.app.name
      com.my.app.name.BroadcastReceiverTest
        Intent Filter:
          Actions:
            - ExportedBroadcastReceiver
        Permission: null

##### Trigger exported broadcast receiver

    dz> run app.broadcast.send --action ExportedBroadcastReceiver --extra string <name> <value> --extra string <name> <value>

Application storage
===================

##### Application directory

    cd /data/data/com.my.app.name
    ls
    app_webview cache databases lib shared_prefs

##### Inspect SQLite database

    cd /data/data/com.my.app.name/databases
    sqlite3 database_file
    sqlite> .tables
    table1 table2
    sqlite> select * from table1;

Java code snippets
==================

##### Java code to invoke exported activity
    package com.example.myapplication;

    import android.content.Intent;
    import android.widget.Button;
    import android.view.View;
    import android.os.Bundle;
    import androidx.appcompat.app.AppCompatActivity;

    public class MainActivity extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            Button mButton = (Button) findViewById(R.id.button);

            mButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent open = new Intent(Intent.ACTION_SEND);
                    open.setClassName("com.my.package.name", "com.my.package.name.exported.activity");
                    startActivity(open);
                }
            });
        }
    }


Android Debug Bridge
====================

##### List devices

    adb devices
    adb devices -l

##### List installed packages

    adb shell 'su -c pm list packages'

##### Remount as rw

    adb remount

##### Restart adb as root (Andoid 10+)

    adb root

##### Reboot device

    adb reboot
    adb reboot bootloader

##### Push file

    adb push </path/to/local/file> </path/to/device/file>

##### Pull file

    adb pull </path/to/device/file> </path/to/local/file>

##### Remote shell

    adb shell
    adb shell <command>

##### Start application

    adb shell monkey -p app.package.name

##### View logs

    adb logcat

##### View logs of specific application

    adb logcat --pid=$(adb shell pidof -s com.my.app.name)

##### Clear log buffer

    adb logcat -c

##### Dump log to file

    adb logcat -f <filename>

##### Get saved WiFi passwords

    cat /data/misc/wifi/wpa_supplicant.conf

##### Install Burp Suite certificate to Android certificate store

    curl --proxy http://127.0.0.1:8080 -o cacert.der http://burp/cert  \
    && openssl x509 -inform DER -in cacert.der -out cacert.pem \
    && cp cacert.der $(openssl x509 -inform PEM -subject_hash_old -in cacert.pem |head -1).0 \
    && adb root \
    && adb remount \
    && adb push $(openssl x509 -inform PEM -subject_hash_old -in cacert.pem |head -1).0 /sdcard/ \
    && echo -n "mv /sdcard/$(openssl x509 -inform PEM -subject_hash_old -in cacert.pem |head -1).0 /system/etc/security/cacerts/" | adb shell \
    && echo -n "chmod 644 /system/etc/security/cacerts/$(openssl x509 -inform PEM -subject_hash_old -in cacert.pem |head -1).0" | adb shell \
    && echo -n "reboot" | adb shell \
    && rm $(openssl x509 -inform PEM -subject_hash_old -in cacert.pem |head -1).0 \
    && rm cacert.pem \
    && rm cacert.der

##### Fix selinux permissions

    restorecon -FR /data/media/0
