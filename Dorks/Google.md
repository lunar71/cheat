Google dorks cheatsheet
=======================

[[_TOC_]]

Common Dorks
============

| Dork name     | Example            |
| ------------- | ------------------ |
| inurl         | inurl:test         |
| intext        | intext:test        |
| contains      | contains:jsp       |
| intitle       | intitle:test       |
| inanchor      | inanchor:test      |
| site          | site:example.com   |
| cache         | cache:example.com  |
| inpostauthor  | inpostauthor:admin |

Operators
=========

| Operator    | Example                              |
| ----------- | ------------------------------------ |
| "term"      | "this is an exact term"              |
| AND / &     | term1 AND term2                      |
| OR / \|     | term1 \| term2                       |
| -           | site:example.com -intext:remove this |
| (operators) | site:example.com ("term1" | "term2") |

